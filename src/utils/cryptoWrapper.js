/**
 * CryptoWrapper for lack of better name
 *


TO DO:
- encrypt credentials with password
- make credentials exportable at registration

*/

/*
	gentle logger
*/
const verbose = 1;
function _log(msg) {
	if(verbose == 0) return;
	window.console.log(msg, arguments);
}


// some utils
import CONST from './constants.js';


//adding zenroom in the mix
import zenroomHelper from './zenroomHelper';
import localforage from 'localforage';

localforage.config({
	name: "CA_store",
	storeName: "CA_keyvaluepairs",
	version: 1
})

/*
 * Note: clean settings up!
 */

let CryptoWrapper = {
	initiated: false,
}

// some settings
const dataSaveKeySettings = {
	algorithm:{
		name: "AES-GCM",
		length: 256
	}, 
	extractable : false,
	keyUsages : ["encrypt","decrypt"]
}

// This is a hack to expose the localforage object also local functions
CryptoWrapper.init = async function(){

	if(this.isInitiated()) return true;

	//expose localforage to private functions

	const dataSaveKey = await generateDataSaveKey();
	if(dataSaveKey) this.initiated = true;

	return new Error("Could not generate a data key pair");
}

CryptoWrapper.isInitiated = function(){
	if(!this.initiated) return false;
	return true;
}

CryptoWrapper.reset =  function(){
	this.initiated = false;
	this.init();
}

CryptoWrapper.storePublicKey = async function(publicKey){
	_log(">> Cryptowrapper > Storing public key",publicKey);
	return localforage.setItem("publicKey",publicKey);
}
CryptoWrapper.getPublicKey = async function(){
	return localforage.getItem("publicKey");
}

CryptoWrapper.getAppCredentials = async function(_user_id){

	const user_id = _user_id || "1";

	//first we check fo the appCredentials are stored
	let encryptedAppCredentials = await localforage.getItem("appCredentials_"+user_id);
	
	//if not, we generate new credientials, encrypt+save, and return.
	if(!encryptedAppCredentials){
		_log(">> CryptoWrapper > no encryptedAppCredentials")
		
		//we generate a fresh pair of credentials
		const appCredentials = await generateAppCredentials(user_id);
		_log(">> CryptoWrapper > generated app credentials",appCredentials);
		
		//we encrypt the credentials and save them
		encryptedAppCredentials = await encryptAndSaveAppCredentials(appCredentials, user_id);
		_log(">> CryptoWrapper > encrypted credentials",encryptedAppCredentials)

		//but we return the unencrypted credentials. 
		return appCredentials;
	}

	//to decrypt the saved keypair, we retreive the key and iv.
	let [key, iv] = await Promise.all([getDataSaveKey(), getIV()]);

	return decrypt(key, iv, encryptedAppCredentials);
 
}

/**
 * Link a new device or restore a session.
 * --
 * We take a privateKey, generate a publicKey
 * Then we encrypt it with the app credentials
 */
CryptoWrapper.verifyAppCredentials = async function(_user_id){
	const user_id = _user_id;
	return localforage.getItem("appCredentials_"+user_id);
}
CryptoWrapper.restoreAppCredentials = async function(_user_id, privateKey, _publicKey){
	
	const user_id = _user_id || "1";
	const publicKey = _publicKey || null;

	let appCredentials = {}
	appCredentials.zenroom 	= CONST.zenroomSettings;
	appCredentials[user_id] = {
		keypair :  {
				public_key: publicKey,
				private_key: "u64:"+privateKey
			}
		}

	window.console.log(">> CryptoWrapper > appCredentials: ", appCredentials);

	let encryptedAppCredentials = await encryptAndSaveAppCredentials(appCredentials, user_id);
	_log(">> CryptoWrapper > encrypted credentials",encryptedAppCredentials)

	return appCredentials;



	// const script = 
	// 		`
	// 		keyring = ECDH.new()
	// 		keyring:private('u64:${privateKey}')

	// 		export = JSON.encode(
	// 			{
	// 				public  = keyring:public():url64(),
	// 				private = keyring:private():url64()
	// 			}
	// 		)
	// 		print(export)`;

	// const ZR = new zenroomHelper();
	// const keyPair = await ZR.runLuaScript({script: script});
	// window.console.log(">> CryptoWrapper > restored credentials",user_id, privateKey, keyPair );
	
	// if(keyPair.publicKey !== publicKey){
	// 	//
	// 	window.console.log("this is not a valid combination")
	// }

	// 	//maybe we should format the keypair?
	// 	encryptedAppCredentials = await encryptAndSaveAppCredentials(appCredentials, user_id);
	// _log(">> CryptoWrapper > encrypted credentials",encryptedAppCredentials)

	// return 

 }


CryptoWrapper.decryptMessageForUser = async function(user_id, msg){

	_log(">> Cryptowrapper > decyrpt message from APP by "+user_id, msg);
	
	try{

		const data = JSON.stringify(msg);

		const userCredentials = await this.getAppCredentials(user_id);
		const public_key = await this.getPublicKey();

		let keys = {
				public_key : {
					APP:  public_key
				},
				zenroom : msg.zenroom,
				...
				userCredentials
			};


		const script = `
			Rule check version 1.0.0
			Scenario 'simple': USER decrypts the message from APP
			Given that I am known as '${user_id}'
			and I have my valid 'keypair'
			and I have a valid 'public key' from 'APP'
			and I have a valid 'secret message'
			When I decrypt the secret message from 'APP'
			Then print as 'string' the 'message'
			and print as 'string' the 'header' inside 'secret message'
			`
		
		// _log("\n\n\n\n\n>>Cryptowrapper > keys\n\n",JSON.stringify(keys),"\n\n")
		// _log("\n>>Cryptowrapper > data\n\n",data,"\n")
		// _log("\n>>Cryptowrapper > script\n\n",script,"\n")
		
		const ZR = new zenroomHelper();
		return ZR.runScript({ script, data, keys });

	} 
	catch(err){
		throw new Error("Could not decrypt ballot");
	}

}
CryptoWrapper.signMessage = async function(_user_id, _msg){

	_log(">> Cryptowrapper > sign message to APP from "+user_id, _msg);
	
	const user_id = _user_id || "1";

	try{

		const msg = JSON.stringify(_msg);
		const data = "";
		const userCredentials = await this.getAppCredentials(user_id);
		const public_key = await this.getPublicKey();

		let keys = {
				public_key : {
					APP:  public_key
				},
				zenroom : msg.zenroom,
				...
				userCredentials
			};


		const script = `
			Rule check version 1.0.0
			Scenario 'simple': USER encrypts a message for APP
			Given that I am known as '${user_id}'
			and I have my valid 'keypair'
			and I have a valid 'public key' from 'APP'
			When I write '${msg}' in 'message'
			and I write 'This is the header' in 'header'
			and I encrypt the message for 'APP'
			Then print the 'secret message'
			`
		
		_log("\n\n\n\n\n>>Cryptowrapper > signMessage > keys\n\n",JSON.stringify(keys),"\n\n")
		_log("\n>>Cryptowrapper > signMessage > script\n\n",script,"\n")
		
		const ZR = new zenroomHelper();
		return ZR.runScript({ script, data, keys });

	} 
	catch(err){
		throw new Error("Could not decrypt ballot");
	}

}
CryptoWrapper.encryptMessageByUser = async function(user_id, _message){

	_log(">> Cryptowrapper > encrypt message from  "+user_id, _message);
	/*
	try{

		const data = JSON.stringify(_message.ballot);

		const userCredentials = await this.getAppCredentials(user_id);
		const public_key = await this.getPublicKey();

		let keys = {
				public_key : {
					APP:  public_key
				},
				zenroom : _message.ballot.zenroom,
				...
				userCredentials
			};


		// const script = `
		// 	Rule check version 1.0.0
		// 	Scenario 'simple': ${user_id} decrypts the message from APP
		// 	Given that I am known as '${user_id}'
		// 	and I have my valid 'keypair'
		// 	and I have a valid 'public key' from 'APP'
		// 	and I have a valid 'secret message'
		// 	When I decrypt the secret message from 'APP'
		// 	Then print as 'string' the 'message'
		// 	and print as 'string' the 'header' inside 'secret message'
			// `
		
		_log("\n\n\n\n\n>>Cryptowrapper > keys\n\n",JSON.stringify(keys),"\n\n")
		_log("\n>>Cryptowrapper > data\n\n",data,"\n")
		_log("\n>>Cryptowrapper > script\n\n",script,"\n")
		
		const ZR = new zenroomHelper();
		return ZR.runScript({
			script: script, 
			data: 	data, 
			keys: 	keys
		})
	} 
	catch(err){
		throw new Error("Could not encrypt ballot");
	}
	*/
}

// /*
//  * Generate data save key
//  */
async function generateDataSaveKey(){

	const dataSaveKey = await localforage.getItem("dataSaveKey");
	if(dataSaveKey) return dataSaveKey;  

	return window.crypto.subtle.generateKey(
		{
			name: "AES-GCM",
			length: 256
		}, 
		dataSaveKeySettings.extractable, 
		dataSaveKeySettings.keyUsages
	)
	.then(_dataSaveKey => {
		return localforage.setItem("dataSaveKey",_dataSaveKey);
	})
	.catch(err => {
		window.console.error(">> err",err);
		return;
	})
}

async function getIV(){

	//check if we have a local random seed
	let iv = await localforage.getItem("iv");
	if(iv) return iv;

	//if not, create it.
	iv = window.crypto.getRandomValues(new Uint8Array(12));

	//and save it...
	return localforage.setItem("iv",iv)

}

async function getDataSaveKey(){
	return localforage.getItem("dataSaveKey");
}


// /*
//  * Generate App credentials
//  */
async function generateAppCredentials(_user_id){

	const user_id = _user_id || "1";

	const params = {
		"script": ` rule check version 1.0.0
					Scenario 'simple': Create the keypair
					Given that I am known as '${user_id}'
					When I create the keypair
					Then print my data`,
		"data": {},
		"keys": {},
		"conf": CONST.zenroomSettings,
	}

	const ZR = new zenroomHelper();
	return ZR.runScript(params);
}

/* Encrypt and Save */

async function encryptAndSaveAppCredentials(msg, _user_id){
	const user_id = _user_id || "1";
	const [key, iv] = await Promise.all([getDataSaveKey(), getIV()]);
	const encrypted = await encrypt(key, iv, msg);
	return localforage.setItem("appCredentials_"+user_id,encrypted);
}


/*
 * Encrypt App credentials 
 */ 
 
async function encrypt(key, iv, msg){
	let encoded = encodeMsg(msg);
	const opts = {name: "AES-GCM",iv: iv};
	return window.crypto.subtle.encrypt(opts,key,encoded);
}

/*
 * Decrypt App credentials
 */
async function decrypt(key, iv, cipherText){
	const opts = {name: "AES-GCM",iv: iv};
	const _encoded = await window.crypto.subtle.decrypt(opts,key,cipherText);
	return decodeMsg(_encoded);
}

/* Encode */
function encodeMsg(_msg) {
	let enc = new TextEncoder();
	let msg = (typeof _msg === "object") ? JSON.stringify(_msg) : _msg;
	return enc.encode(msg);
}
/* Decode */
function decodeMsg(_msg){

	let dec = new TextDecoder();
	let msg = dec.decode(_msg);
	let msgObj = {};

	try{
		msgObj = JSON.parse(msg);
	}
	catch(err){
		window.console.error("CryptoWrapper >> msg was not an object > ",err);
		msgObj.msg = msg;
	}
	return msgObj;
}



const CryptoWrapperPlugin = {
	install(Vue){
		Vue.prototype.$CryptoWrapper = CryptoWrapper;
	}
}

export default CryptoWrapperPlugin;









// CryptoWrapper.generateAppCredentials = async function(){

// 	//now we generate a new appCredentials with zenroom
// 	this.appCredentials = await generateAppCredentials();

// 	//and encrypt it with the dataSaveKey
// 	let encryptedCredentials = await encryptAppCredentials(this.dataSaveKey, this.iv, this.appCredentials);

// 	//and then we save the credentials in the indexedDB
// 	_log(">> cryptoWrapper > encrypted key: ", encryptedCredentials);

// 	//for good measure, we also decrypt it
// 	let decrypted = await decryptAppCredentials(this.dataSaveKey, this.iv, encryptedCredentials);

// 	_log(">> cryptoWrapper > decrypted key: ", decrypted);

// }

/*
 * Export the App credentials to back them up on anoter device
 */
// CryptoWrapper.exportAppCredentials = async function(){

// 	// const key = await localforage.getItem("dataSaveKey");
// 	// const iv  = await localforage.getItem("iv");
// 	// const cipher = await localforage.getItem("appCredentials");
	
// 	// return decryptAppCredentials(key, iv, cipher);

// }

// CryptoWrapper.importAppCredentials = async function(_cipher){
// 	if(_cipher) return;
// }
