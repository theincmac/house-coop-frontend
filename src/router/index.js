import Vue from 'vue'
import VueRouter from 'vue-router'

import CAWelcome from '../components/CAWelcome.vue'
import CAMembers from '../components/CAMembers.vue'
import CAMember from '../components/CAMember.vue'

import CACategories from '../components/CACategories.vue'
import CACategory from '../components/CACategory.vue'
import CAEditCategory from '../components/CAEditCategory.vue'

import CAArticle from '../components/CAArticle.vue'
import CAEditArticle from '../components/CAEditArticle.vue'

import CALogin from '../components/CALogin.vue'
import CARegister from '../components/CARegister.vue'
import CAAccount from '../components/CAAccount.vue'

import CAExplainKeys from '../components/CAExplainKeys.vue'
import CAMilestones from '../components/CAMilestones.vue'
import CAEditMilestone from '../components/CAEditMilestone.vue'
import CAMilestone from '../components/CAMilestone.vue'

import CASetup from '../components/CASetup.vue'

// import CATools from '../components/CATools.vue'
// import CASettings from '../components/CASettings.vue'



Vue.use(VueRouter)

export default new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'welcome',
			component: CAWelcome
		},
		{
			path: '/home',
			name: 'home',
			component: CAWelcome
		},
			{
			path: '/members',
			name: 'members',
			component: CAMembers
		},
		{
			path: '/member/:id',
			name: 'member',
			component: CAMember
		},
		{
			path: '/categories',
			name: 'categories',
			component: CACategories
		},
		{
			path: '/milestones',
			name: 'milestones',
			component: CAMilestones
		},
		{
			path: '/milestone',
			name: 'milestone',
			component: CAMilestone
		},
		{
			path: '/milestone/new',
			name: 'newMilestone',
			component: CAEditMilestone
		},
		{
			path: '/milestone/:id/edit',
			name: 'editMilestone',
			component: CAEditMilestone
		},
		{
			path: '/category/new',
			name: 'newCategory',
			component: CAEditCategory
		},
		{
			path: '/category/:id/edit',
			name: 'editCategory',
			component: CAEditCategory,
			props: true
		},
		{
			path: '/category/:id',
			name: 'category',
			component: CACategory
		},
		{
			path: '/articles/:id',
			name: 'article',
			component: CAArticle
		},
		{
			path: '/articles/:id/edit',
			name: 'editArticle',
			component: CAEditArticle,
			props: true
		},
		{
			path: '/category/:category/new-article',
			name: 'newArticle',
			component: CAEditArticle,
			props: true
		},

		{
			path: '/login',
			name: 'login',
			component: CALogin
		},
		{
			path: '/register',
			name: 'register',
			component: CARegister
		},
		{
			path: '/account/',
			name: 'account',
			component: CAAccount,
		},
		{
			path: '/explainkeys/',
			name: 'explainkeys',
			component: CAExplainKeys,
		},
		{
			path: '/setup/',
			name: 'setup',
			component: CASetup,
		}
	]
})