# House.coop Frontend

Front-end made in Vue.js to go with House.coop App (https://bitbucket.org/theincmac/house-coop-app/src).
Only use this repository if you want to make custom adjustments to the UI.

## Project setup
```bash
$ git clone git@bitbucket.org:theincmac/house-coop-frontend.git
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
